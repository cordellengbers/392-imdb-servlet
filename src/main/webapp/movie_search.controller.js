var app = angular.module ("imdb");

app.controller('MovieSearchCtrl', function($scope, $http) {
	$scope.years = [];
    for ($i = 1940; $i <= 2010; $i++) {
    	$scope.years.push($i);
    }
    
    $scope.year = -1;
    $scope.title = "";
    $scope.director = "";
    $scope.runningTimeComparator = "<=";
    
    $scope.movies = [];
    $scope.searchPerformed = false;
     
    $scope.searchDisabled = function () {
    	return $scope.title.length == 0 && $scope.director.length == 0 && $scope.year == -1 && $scope.director.length == 0 && (!$scope.runningTime || $scope.runningTime.length == 0);
    }
    
    $scope.searchForMovies = function () {
    	
    	
    	var searchParameters = {
    		title: "",
    		primaryGenre: "",
    		year: $scope.year,
    		director: "",
    		runningTimeComparator: $scope.runningTimeComparator,
    		runningTime: -1
    	};
    	
    	if ($scope.title.length > 0) {
    		searchParameters.title = $scope.title;
    	}
    	
    	if ($scope.primaryGenre && $scope.primaryGenre != "null") {
    		searchParameters.primaryGenre = $scope.primaryGenre;
    	}
    	
    	if ($scope.director.length > 0) {
    		searchParameters.director = $scope.director;
    	}
    	
    	if ($scope.runningTime && $scope.runningTime.length > 0) {
    		searchParameters.runningTime = $scope.runningTime;
    	}
    	
    	$http.post("api/movies/search", searchParameters).then(
    			function (response) {
    				$scope.searchPerformed = true;
    				$scope.movies = response.data;
    			}
    		);
    };
        
    $http.get("api/genres").then(
    	function (response) {
    		$scope.genres = response.data;
    	}
    );
    
});